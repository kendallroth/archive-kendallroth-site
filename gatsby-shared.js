const React = require('react');
const PropTypes = require('prop-types');
const { ThemeProvider } = require('styled-components');

// Utilities
const styledThemeModule = require('@utils/theme');

const {
  customizableThemePalette,
  default: styledTheme,
  ThemeOverrideContext,
} = styledThemeModule;

/**
 * React wrapper around Gatsby app wrapper (since Gatsby React fails Hook validation...)
 */
const AppWrapper = ({ children }) => {
  const [palette, setPalette] = React.useState(customizableThemePalette);
  const [originalPalette] = React.useState(customizableThemePalette);

  /**
   * Reset the site background
   */
  const resetBackground = () => {
    setPalette({
      ...palette,
      background: originalPalette.background,
    });
  };

  /**
   * Update the site background
   */
  const updateBackground = (paletteBackground) => {
    setPalette({
      ...palette,
      background: paletteBackground,
    });
  };

  const mergedTheme = {
    ...styledTheme,
    palette: {
      ...styledTheme.palette,
      ...palette,
    },
  };

  return (
    <ThemeProvider theme={mergedTheme}>
      <ThemeOverrideContext.Provider
        value={{ resetBackground, updateBackground }}
      >
        {children}
      </ThemeOverrideContext.Provider>
    </ThemeProvider>
  );
};

AppWrapper.propTypes = {
  children: PropTypes.element.isRequired,
};

exports.default = AppWrapper;
