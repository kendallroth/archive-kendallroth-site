import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';

// Styles
import { PageSC } from '@utils/styled';

/**
 * Project page template (from markdown)
 * @param {object} data      - GraphQL data
 * @param {object} data.file - Project page markdown file
 */
const ProjectTemplate = (props) => {
  const { data } = props;
  const { file } = data;
  const { childMarkdownRemark } = file;
  const { frontmatter, html } = childMarkdownRemark;

  return (
    <PageSC>
      <div className="grid-container">
        <div className="grid-x grid-margin-x">
          <div className="cell small-12 medium-offset-1 medium-10">
            <h1>{frontmatter.title}</h1>
            <div
              className="blog-post-content"
              dangerouslySetInnerHTML={{ __html: html }}
            />
          </div>
        </div>
      </div>
    </PageSC>
  );
};

export const pageQuery = graphql`
  query ProjectPageByPath($relativePath: String!) {
    file(relativePath: { eq: $relativePath }) {
      name
      childMarkdownRemark {
        html
        frontmatter {
          title
        }
      }
    }
  }
`;

ProjectTemplate.propTypes = {
  data: PropTypes.shape({
    file: PropTypes.shape({
      childMarkdownRemark: PropTypes.shape({
        frontmatter: PropTypes.shape({
          title: PropTypes.string,
        }).isRequired,
        html: PropTypes.node.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};

export default ProjectTemplate;

/* eslint react/no-danger: off */
