---
collaborators:
  - doug-epp
image: captain_cpa.png
published: true
slug: captain-cpa
status: Completed
title: Captain CPA
year: 2015
---

Captian CPA is a platformer game created in XNA Game Studio by two second year Conestoga Computer Programmer/Analyst students for a final project in Object Orientated Game Programming.

> YOU ARE CAPTAIN CPA, A RENOWNED HERO WHOSE STRENGTH, SPEED, AND JUMPING ABILITY ARE KNOWN THE WORLD OVER! TRAVERSE A HOSTILE WORLD, COLLECT DISCS, AND AVOID ENEMIES TO REACH YOUR GOAL!

## Collaborators

- [Doug Epp](https://github.com/DougEpp)
- [Kendall Roth](https://github.com/kendallroth)
