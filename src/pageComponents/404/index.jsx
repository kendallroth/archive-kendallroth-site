import React from 'react';

// Components
import DefaultLayout from '@layouts/DefaultLayout';

// Styles
import { PageSC } from '@utils/styled';
import { HomePageLinkSC, NotFoundSC, NotFoundTitleSC } from './styled';
import { HeroContainerSC } from '../index/styled';

/**
 * 404 page
 */
const NotFoundPage = () => (
  <DefaultLayout>
    <PageSC offsetTop={false}>
      <HeroContainerSC>
        <NotFoundSC>
          <NotFoundTitleSC>404</NotFoundTitleSC>
          <p>
            You just hit a route that doesn&lsquo;t exist&hellip;the sadness.
          </p>
          <p>
            <small style={{ opacity: 0.6 }}>
              P.S. If this was the &ldquo;About&rdquo;
              route&hellip;it&nbsp;was&nbsp; intentional.
            </small>
          </p>
          <HomePageLinkSC>Go to Home</HomePageLinkSC>
        </NotFoundSC>
      </HeroContainerSC>
    </PageSC>
  </DefaultLayout>
);

export default NotFoundPage;
