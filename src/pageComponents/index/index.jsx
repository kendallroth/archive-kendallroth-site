import React, { useContext, useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { parse as parseDate } from 'date-fns';
import { ThemeContext } from 'styled-components';

// Components
import EmploymentSection from './EmploymentSection';
import BottomWaves from '@components/BottomWaves';
import Footer from '@components/Footer';
import MessageBar from '@components/MessageBar';
import DefaultLayout from '@layouts/DefaultLayout';
import ProjectsSection from './ProjectsSection';
import SkillsSection from './SkillsSection';
import WhatIDoSection from './WhatIDoSection';

// Utils
import { mapFileToImageFromQuery } from '@utils/mapper';

// Styles
import { PageSC } from '@utils/styled';
import {
  ContactMeButtonSC,
  ContactMeSC,
  HeroButtonSC,
  HeroContainerSC,
  HeroNameSC,
  HeroTextSC,
} from './styled';

// Data
import skillsJson from '@data/skills.json';

/**
 * Index page
 */
const IndexPage = (props) => {
  const [employers, setEmployers] = useState([]);
  const [projects, setProjects] = useState([]);
  const theme = useContext(ThemeContext);
  const firstSectionRef = useRef(null);

  useEffect(() => {
    setEmployers(matchEmployersWithImagesFromProps());
    setProjects(matchProjectsWithImagesFromProps());
  }, [props.data]);

  /**
   * Scroll to the beginning of the online portfolio
   */
  const onPortfolioClick = () => {
    const element = firstSectionRef.current;
    const headerOffset = theme.header.height;
    const elementPosition =
      element.getBoundingClientRect().top + window.scrollY;
    const offsetPosition = elementPosition - headerOffset;

    window.scrollTo({
      top: offsetPosition,
      behavior: 'smooth',
    });
  };

  /**
   * Match employers with their images (queried separately)
   * @param  {object}   props - React props (employer and image edges)
   * @return {object[]}       - List of employers with images
   */
  // eslint-disable-next-line class-methods-use-this
  const matchEmployersWithImagesFromProps = () => {
    const { data } = props;
    const { allEmployerImages, allEmployerFiles } = data;

    if (!allEmployerImages || !allEmployerImages.edges) return [];
    if (!allEmployerFiles || !allEmployerFiles.edges) return [];

    // TODO: Fix sort function
    // .sort((obj1, obj2) => (obj2.year - obj1.year || obj1.title > obj2.title ? 1 : -1))
    return mapFileToImageFromQuery(
      allEmployerFiles.edges,
      allEmployerImages.edges
    )
      .map((e) => ({ ...e, company: e.title }))
      .sort(
        (obj1, obj2) =>
          parseDate(obj2.dates.end || new Date(), 'MMM YYYY') -
          parseDate(obj1.dates.end || new Date(), 'MMM YYYY')
      );
  };

  /**
   * Match projects with their images (queried separately)
   * @param  {object}   props - React props (project and image edges)
   * @return {object[]}       - List of projects with images
   */
  // eslint-disable-next-line class-methods-use-this
  const matchProjectsWithImagesFromProps = () => {
    const { data } = props;
    const { allProjectImages, allProjectFiles } = data;

    if (!allProjectImages || !allProjectImages.edges) return [];
    if (!allProjectFiles || !allProjectFiles.edges) return [];

    // TODO: Fix sort function
    // .sort((obj1, obj2) => (obj2.year - obj1.year || obj1.title > obj2.title ? 1 : -1))
    return mapFileToImageFromQuery(
      allProjectFiles.edges,
      allProjectImages.edges
    )
      .map((p) => ({ ...p, name: p.title }))
      .sort((obj1, obj2) => obj2.year - obj1.year);
  };

  const nameColor = theme.palette.secondary.main;

  return (
    <DefaultLayout>
      <PageSC offsetTop={false}>
        <HeroContainerSC>
          <BottomWaves />
          <HeroTextSC>
            <span>
              Hi, I&rsquo;m&nbsp;
              <HeroNameSC color={nameColor}>Kendall Roth</HeroNameSC>.<br />
              <small style={{ opacity: 0.7 }}>
                I&rsquo;m a full-stack web developer.
              </small>
            </span>
            <HeroButtonSC onClick={onPortfolioClick}>
              View My Portfolio
              <i className="material-icons">work</i>
            </HeroButtonSC>
          </HeroTextSC>
        </HeroContainerSC>
        <MessageBar borderTop={theme.palette.primary.main}>
          Welcome to my online portfolio!
        </MessageBar>
        <WhatIDoSection id="what-i-do" sectionRef={firstSectionRef} />
        <ProjectsSection projects={projects} />
        <SkillsSection skills={skillsJson} />
        <EmploymentSection employers={employers} />
        <ContactMeSC>
          <ContactMeButtonSC href="mailto:kendall@kendallroth.ca">
            Contact Me
          </ContactMeButtonSC>
        </ContactMeSC>
        <Footer showSocial />
      </PageSC>
    </DefaultLayout>
  );
};

IndexPage.propTypes = {
  data: PropTypes.shape({
    allProjectFiles: PropTypes.shape({
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            childMarkdownRemark: PropTypes.shape({
              frontmatter: PropTypes.object.isRequired,
            }).isRequired,
          }),
        })
      ),
    }),
    allEmployerFiles: PropTypes.shape({
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            childMarkdownRemark: PropTypes.shape({
              frontmatter: PropTypes.object.isRequired,
            }).isRequired,
          }),
        })
      ),
    }),
  }),
};

IndexPage.defaultProps = {
  data: {},
};

export default IndexPage;
