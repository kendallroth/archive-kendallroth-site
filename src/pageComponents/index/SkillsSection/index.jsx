import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FlipMove from 'react-flip-move';

// Components
import TechnologyCard from '@components/TechnologyCard';

// Styles
import {
  SectionHeaderSC,
  ShuffleButtonSC,
  SkillsSC,
  TagsRowSC,
  TagSC,
  TechnologyRowSC,
} from './styled';
import './styles.css';

/**
 * Skills section
 */
class SkillsSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTag: null,
      tags: this.mapPropsToTags(props),
      // NOTE: Shuffling immediately causes some strange issues where images
      //         and ratings do not align with proper technologies (random).
      technologies: props.skills.technologies,
    };
  }

  onComponentWillReceiveProps(nextProps) {
    this.setState({
      tags: this.mapPropsToTags(nextProps),
    });
  }

  /**
   * Shuffle the technology cards
   */
  onShuffleClick = () => {
    const { technologies } = this.state;

    this.setState({
      technologies: this.shuffleTechnologies(technologies),
    });
  };

  /**
   * Set the current tag (highlights matching technologies)
   */
  onTagMouseEnter = (tag) => {
    this.setState({
      currentTag: tag,
    });
  };

  /**
   * Unset the current tag
   */
  onTagMouseLeave = () => {
    this.setState({
      currentTag: null,
    });
  };

  /**
   * Create a list of available technology tags (de-duplicated)
   */
  mapPropsToTags = (props) =>
    props.skills.technologies.reduce((accum, technology) => {
      // Only add "new" tags (to avoid duplication)
      const newTags = technology.tags.filter((tag) => !accum.includes(tag));

      return [...accum, ...newTags];
    }, []);

  /**
   * Shuffle the list of technology cards (Durstenfeld shuffle)
   * @param  {object[]} technologies - List of technologies
   * @return {object[]}              - Shuffled list of technologies
   */
  shuffleTechnologies = (technologies) => {
    const array = [...technologies];

    // eslint-disable-next-line no-plusplus
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }

    return array;
  };

  render() {
    const { currentTag, tags, technologies } = this.state;

    return (
      <SkillsSC>
        <SectionHeaderSC>
          What I&lsquo;ve Worked With
          <ShuffleButtonSC onClick={this.onShuffleClick}>
            <i className="material-icons">shuffle</i>
          </ShuffleButtonSC>
        </SectionHeaderSC>
        <div className="grid-container full-width">
          <div className="grid-x show-for-medium">
            <TagsRowSC className="cell small-12">
              {tags.map((t) => (
                <TagSC
                  key={t}
                  onMouseEnter={() => this.onTagMouseEnter(t)}
                  onMouseLeave={() => this.onTagMouseLeave(t)}
                >
                  {t}
                </TagSC>
              ))}
            </TagsRowSC>
          </div>
          <div className="grid-x">
            <TechnologyRowSC className="cell small-12">
              <FlipMove
                className="flip-move--technologies"
                duration={1000}
                delay={0}
                staggerDurationBy={15}
                staggerDelayBy={20}
              >
                {technologies.map((t) => {
                  const isSelected = t.tags.includes(currentTag);

                  return (
                    <TechnologyCard
                      key={t.title}
                      image={t.image}
                      rating={t.rating}
                      selected={isSelected}
                      tags={t.tags}
                      title={t.title}
                    />
                  );
                })}
              </FlipMove>
            </TechnologyRowSC>
          </div>
        </div>
      </SkillsSC>
    );
  }
}

SkillsSection.propTypes = {
  skills: PropTypes.shape({
    technologies: PropTypes.arrayOf(
      PropTypes.shape({
        description: PropTypes.string,
        image: PropTypes.string,
        rating: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string),
      })
    ),
  }),
};

SkillsSection.defaultProps = {
  skills: [],
};

export default SkillsSection;

/* eslint react/no-array-index-key: off */
