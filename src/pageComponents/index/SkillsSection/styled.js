import styled from 'styled-components';

// Components
import { TechnologyCardSC } from '@components/TechnologyCard/styled';

// Utils
import { MediaQuery } from '@utils/mediaQuery';

/**
 * Section header
 */
const SectionHeaderSC = styled.h2`
  position: relative;
  display: flex;
  align-items: center;
  font-size: 1.75em;
  text-align: center;
`;

/**
 * Shuffle skill cards button
 */
const ShuffleButtonSC = styled.button`
  position: absolute;
  right: -36px;
  display: flex;
  padding: 4px;
  border-radius: 100%;
  cursor: pointer;
  transition: background-color 0.1s ease-in-out;

  &:hover {
    background-color: #e0e0e0;
  }

  i {
    font-size: 20px;
  }
`;

/**
 * Section describing my skill set
 */
const SkillsSC = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem 0;
`;

/**
 * Technology filter tag
 */
const TagSC = styled.span`
  padding: 0.25em 0.75em;
  font-size: 13px;
  border-radius: 4px;
  background-color: #e8eaf6;
  cursor: pointer;
  border: 1px solid #9fa8da;
`;

/**
 * Technology filter tag row
 */
const TagsRowSC = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-bottom: 1em;

  ${TagSC}:not(:first-of-type) {
    margin-left: 1em;
  }
`;

/**
 * Technology skills row
 */
const TechnologyRowSC = styled.div`
  ${MediaQuery.phoneOnly`
    overflow: hidden;
    border-radius: 4px;
    box-shadow: ${({ theme }) => theme.shadows[4]};
  `};

  ${TechnologyCardSC} {
    width: 100%;
    margin: 8px;

    /* Drastically different styling for phone screens */
    ${MediaQuery.phoneOnly`
      margin: 0;
      border-radius: 0;
      box-shadow: none;

      /* Remove hover styles on mobile */
      &:hover {
        transform: none;
        box-shadow: ${({ theme }) => theme.shadows[1]};
      }
    `};

    ${MediaQuery.smallUp`
      width: calc(100% / 2 - ${8 * 2}px);
    `};

    ${MediaQuery.tabletUp`
      width: calc(100% / 3 - ${8 * 2}px);
    `};

    ${MediaQuery.desktopUp`
      margin: 12px;
      width: calc(100% / 4 - ${12 * 2}px);
    `};
  }
`;

export { SectionHeaderSC, ShuffleButtonSC, SkillsSC, TagsRowSC, TagSC, TechnologyRowSC };
