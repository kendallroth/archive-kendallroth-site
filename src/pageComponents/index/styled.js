import styled from 'styled-components';

// Utils
import { MediaQuery } from '@utils/mediaQuery';

/**
 * Contact Me button
 */
const ContactMeButtonSC = styled.a`
  padding: 1.25em;
  color: white;
  border: 3px double white;
  border-radius: 2px;
  text-decoration: none;

  transition: background-color 0.25s, border-radius 0.1s;

  &:hover {
    border-radius: 100px;
    border-style: solid;
    background-color: rgba(255, 255, 255, 0.1);
  }
`;

/**
 * Contact Me section wrapper
 */
const ContactMeSC = styled.section`
  display: flex;
  flex-direction: center;
  justify-content: center;
  padding: 2.5rem;
  background-color: rgb(40, 40, 40);
`;

/**
 * Hero "portfolio" button
 */
const HeroButtonSC = styled.button`
  display: flex;
  align-items: center;
  margin-top: 32px;
  padding: 12px;
  color: white;
  font-size: 0.6em;
  border: 3px double white;
  border-radius: 2px;
  cursor: pointer;

  ${MediaQuery.tabletUp`
    font-size: 0.4em;
  `}

  transition: border 0.25s;

  &:hover {
    border-style: solid;
    border-color: ${({ theme }) => theme.palette.primary.main};
    background-color: ${({ theme }) => theme.palette.primary.main};
  }

  .material-icons {
    position: relative;
    top: 4px;
    margin-left: 8px;
    margin-top: -8px;
  }
`;

/**
 * Hero image container
 */
const HeroContainerSC = styled.section`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;

  &:after {
    content: '';
    background-color: ${({ theme }) => theme.palette.background.background};
    background-image: ${({ theme }) =>
      `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='400' height='400' viewBox='0 0 800 800'%3E%3Cg fill='none' stroke='%23${theme.palette.background.lines.substring(
        1
      )}' stroke-width='1'%3E%3Cpath d='M769 229L1037 260.9M927 880L731 737 520 660 309 538 40 599 295 764 126.5 879.5 40 599-197 493 102 382-31 229 126.5 79.5-69-63'/%3E%3Cpath d='M-31 229L237 261 390 382 603 493 308.5 537.5 101.5 381.5M370 905L295 764'/%3E%3Cpath d='M520 660L578 842 731 737 840 599 603 493 520 660 295 764 309 538 390 382 539 269 769 229 577.5 41.5 370 105 295 -36 126.5 79.5 237 261 102 382 40 599 -69 737 127 880'/%3E%3Cpath d='M520-140L578.5 42.5 731-63M603 493L539 269 237 261 370 105M902 382L539 269M390 382L102 382'/%3E%3Cpath d='M-222 42L126.5 79.5 370 105 539 269 577.5 41.5 927 80 769 229 902 382 603 493 731 737M295-36L577.5 41.5M578 842L295 764M40-201L127 80M102 382L-261 269'/%3E%3C/g%3E%3Cg fill='%23${theme.palette.background.dots.substring(
        1
      )}'%3E%3Ccircle cx='769' cy='229' r='5'/%3E%3Ccircle cx='539' cy='269' r='5'/%3E%3Ccircle cx='603' cy='493' r='5'/%3E%3Ccircle cx='731' cy='737' r='5'/%3E%3Ccircle cx='520' cy='660' r='5'/%3E%3Ccircle cx='309' cy='538' r='5'/%3E%3Ccircle cx='295' cy='764' r='5'/%3E%3Ccircle cx='40' cy='599' r='5'/%3E%3Ccircle cx='102' cy='382' r='5'/%3E%3Ccircle cx='127' cy='80' r='5'/%3E%3Ccircle cx='370' cy='105' r='5'/%3E%3Ccircle cx='578' cy='42' r='5'/%3E%3Ccircle cx='237' cy='261' r='5'/%3E%3Ccircle cx='390' cy='382' r='5'/%3E%3C/g%3E%3C/svg%3E")`};
    /* Background by SVGBackgrounds.com */
    background-attachment: fixed;
    opacity: 1;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    right: 0;
    position: absolute;
    z-index: -1;
  }
`;

/**
 * Hero text name
 */
const HeroNameSC = styled.span`
  color: ${({ color }) => color};
  font-weight: bold;
`;

/**
 * Hero image text
 */
const HeroTextSC = styled.h1`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 400px;
  margin: 2em 1em;
  color: white;
  text-align: center;
  line-height: 1.5em;
  font-weight: normal;
  font-size: 1.75em;
  /* Display over waves */
  z-index: 5;

  ${MediaQuery.tabletUp`
    max-width: 500px;
    font-size: 2.25em;
  `};

  ${MediaQuery.desktopUp`
    max-width: 600px;
    font-size: 2.5em;
  `};

  ${MediaQuery.largeUp`
    max-width: 750px;
    font-size: 3em;
  `};

  ${MediaQuery.hugeUp`
    max-width: 1250px;
    font-size: 5em;
  `};
`;

export {
  ContactMeButtonSC,
  ContactMeSC,
  HeroButtonSC,
  HeroContainerSC,
  HeroNameSC,
  HeroTextSC,
};
