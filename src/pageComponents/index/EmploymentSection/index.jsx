import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';

// Components
import EmploymentPanel from '@components/EmploymentPanel';
import SectionHeader from '@components/SectionHeader';

// Styles
import {
  EmploymentGridSC,
  EmploymentSectionSC,
  PanelDividerSC,
} from './styled';

/**
 * Employment section on Home page
 * @param {object[]} employers - List of employment positions and employers
 */
const EmploymentSection = (props) => {
  const { employers } = props;

  return (
    <EmploymentSectionSC>
      <SectionHeader color="white">Where I&rsquo;ve Worked</SectionHeader>
      <EmploymentGridSC className="grid-container full-width">
        <div className="grid-x grid-margin-x">
          <div className="cell small-12 medium-10 medium-offset-1 large-8 large-offset-2">
            <EmploymentPanel
              company="[ Your Name ]"
              dates={{ end: '?', start: '?' }}
              hirePrompt
              image={null}
              position="[ Interesting Title ]"
              key="hirePrompt"
            />
            <PanelDividerSC />
            {employers.map((employer) => {
              const { dates, company, imageSize, position, slug } = employer;

              return (
                <EmploymentPanel
                  company={company}
                  dates={dates}
                  imageSize={imageSize}
                  position={position}
                  key={slug}
                />
              );
            })}
          </div>
        </div>
      </EmploymentGridSC>
    </EmploymentSectionSC>
  );
};

EmploymentSection.propTypes = {
  employers: PropTypes.arrayOf(
    PropTypes.shape({
      company: PropTypes.string.isRequired,
      imageSize: PropTypes.shape({
        originalName: PropTypes.string.isRequired,
      }),
      slug: PropTypes.string.isRequired,
      website: PropTypes.string.isRequired,
    })
  ),
};

EmploymentSection.defaultProps = {
  employers: [],
};

export const fragment = graphql`
  fragment EmployerSummaryFileFragment on FileConnection {
    edges {
      node {
        childMarkdownRemark {
          frontmatter {
            dates {
              end
              start
            }
            image
            location
            published
            position
            slug
            title
            website
          }
        }
      }
    }
  }

  fragment EmployerSummaryImageFragment on FileConnection {
    edges {
      node {
        childImageSharp {
          sizes {
            # Need to include query from gatsby-images for Image component
            ...GatsbyImageSharpSizes
            originalName
          }
        }
      }
    }
  }
`;

export default EmploymentSection;
