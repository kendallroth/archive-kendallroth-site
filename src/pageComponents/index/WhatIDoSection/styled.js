import styled from 'styled-components';

// Utils
import { MediaQuery, QuerySizes } from '@utils/mediaQuery';

// Styles
import { OverviewCardSC } from '@components/OverviewCard/styled';

/**
 * Row of description cards (for "What I Do")
 */
const CardRowSC = styled.div`
  display: flex;
  justify-content: center;
  align-items: stretch;
  padding: 0 0.5em;

  ${MediaQuery.smallDown`
    max-width: ${QuerySizes.small * 0.75}px;
    padding: 0 1.5em;
    flex-direction: column;

    ${OverviewCardSC}:not(: last-of-type) {
      margin-bottom: 1em;
    }
  `};

  ${MediaQuery.tabletUp`
    flex-direction: row;
    margin-bottom: 0;

    ${OverviewCardSC} {
      margin: 0 0.5em;
      max-width: 22em;
    }
  `};
`;

/**
 * Section describing what I do
 */
const WhatIDoSC = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem 0;
`;

/**
 * Section header
 */
const SectionHeaderSC = styled.h2`
  font-size: 1.75em;
  text-align: center;
`;

/**
 * Section quote
 */
const SectionQuoteSC = styled.blockquote`
  max-width: 50em;
  margin: 0 1.5em 2em 1.5em;
  color: #888888;

  ${MediaQuery.phoneOnly`
    padding: 0.75em 1em;
    text-align: center;
    border-top: 1px solid #cecece;
    border-bottom: 1px solid #cecece;
  `};

  ${MediaQuery.tabletUp`
    padding: 0.75em;
    font-size: 1.1em;
    border-left: 1px solid #cecece;
  `};
`;

export { CardRowSC, SectionHeaderSC, SectionQuoteSC, WhatIDoSC };
