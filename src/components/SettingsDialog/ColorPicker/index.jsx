import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { ColorIndicatorSC, ColorPickerRowSC } from './styled';

/**
 * Color indicator (for color picker)
 */
const ColorPicker = (props) => {
  const { active, color, onClick } = props;

  return (
    <ColorIndicatorSC
      active={active}
      color={color}
      maxLength="7"
      onClick={onClick}
    >
      {color}
    </ColorIndicatorSC>
  );
};

ColorPicker.propTypes = {
  active: PropTypes.bool,
  color: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

ColorPicker.defaultProps = {
  active: false,
};

export default ColorPicker;
export { ColorPickerRowSC };
