import styled from 'styled-components';

// Circuit board image container
const CircuitBoardSC = styled.div`
  position: relative;
  height: ${({ height }) => height};
  width: ${({ width }) => width};
`;

// Circuit board children container (hides over-wrap)
const CircuitBoardChildrenSC = styled.div`
  position: absolute;
  top: ${({ offsetPercent }) => offsetPercent}%;
  right: ${({ offsetPercent }) => offsetPercent}%;
  bottom: ${({ offsetPercent }) => offsetPercent}%;
  left: ${({ offsetPercent }) => offsetPercent}%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: ${({ radius }) => radius};
  overflow: hidden;

  img {
    margin: 0;
  }
`;

export { CircuitBoardSC, CircuitBoardChildrenSC };
