import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  ProgressBarSC,
  ProgressRowSC,
  TechnologyCardSC,
  TechnologyCardContentSC,
  TechnologyCardImageSC,
  TechnologyCardTagSC,
  TechnologyCardTagsSC,
  TechnologyCardTitleSC,
} from './styled';

/**
 * Card-like display for technology (skill)
 * @param {string}   className - HTML class
 * @param {string}   image     - Skill image (filename)
 * @param {boolean}  selected  - Whether the card is selected
 * @param {number}   rating    - Skill/comfort rating
 * @param {string[]} tags      - Skill tags
 * @param {string}   title     - Card title
 */
class TechnologyCard extends Component {
  // NOTE: Component must be a class due to Animation library used (react-flip-move)
  render() {
    const { className, image, rating, selected, tags, title } = this.props;

    let logo;
    let noLogo = false;

    try {
      // eslint-disable-next-line global-require, import/no-dynamic-require
      logo = require(`@data/images/skills/${image}.png`);
    } catch (e) {
      // eslint-disable-next-line global-require, import/no-dynamic-require
      logo = require('@data/images/skills/empty.png');
      noLogo = true;
    }

    return (
      <TechnologyCardSC className={className} selected={selected}>
        <TechnologyCardImageSC
          alt={`Skill - ${title}`}
          data-broken={noLogo}
          src={logo}
        />
        <TechnologyCardContentSC>
          <TechnologyCardTitleSC>{title}</TechnologyCardTitleSC>
          <TechnologyCardTagsSC>
            {tags.map((t, idx) => (
              <TechnologyCardTagSC key={idx}> {t} </TechnologyCardTagSC>
            ))}
          </TechnologyCardTagsSC>
        </TechnologyCardContentSC>
        <ProgressRowSC>
          <ProgressBarSC style={{ width: `${rating}%` }} />
        </ProgressRowSC>
      </TechnologyCardSC>
    );
  }
}

TechnologyCard.propTypes = {
  className: PropTypes.string,
  image: PropTypes.string,
  rating: PropTypes.number.isRequired,
  selected: PropTypes.bool,
  tags: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
};

TechnologyCard.defaultProps = {
  className: '',
  image: null,
  selected: false,
  tags: [],
};

export default TechnologyCard;

/* eslint react/no-array-index-key: off, react/prefer-stateless-function: off */
