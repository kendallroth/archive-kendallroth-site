import React, { useState } from 'react';

// Styles
import {
  HeaderSC,
  HeaderLogoSC,
  HeaderMenuLeftSC,
  HeaderMenuRightSC,
  HeaderSettingsSC,
} from './styled';

// Components
import HeaderMenuLink from './HeaderMenuLink';
import SettingsDialog from '../SettingsDialog';

// Assets
import logo from '@assets/icons/logo_light_circle.svg';

/**
 * Fixed site header
 */
const Header = () => {
  const [isSettingsOpen, setSettingsOpen] = useState(false);

  return (
    <HeaderSC>
      <HeaderMenuLeftSC>
        <HeaderMenuLink to="/">Home</HeaderMenuLink>
      </HeaderMenuLeftSC>
      <HeaderMenuRightSC>
        <HeaderMenuLink to="/about">About</HeaderMenuLink>
      </HeaderMenuRightSC>
      <HeaderLogoSC src={logo} />
      <HeaderSettingsSC
        className="material-icons show-for-medium"
        onClick={() => setSettingsOpen(true)}
      >
        settings
      </HeaderSettingsSC>
      {isSettingsOpen && (
        <SettingsDialog onClose={() => setSettingsOpen(false)} />
      )}
    </HeaderSC>
  );
};

export default Header;
