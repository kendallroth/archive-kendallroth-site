import styled from 'styled-components';

// Styles
import theme from '@utils/theme';

/**
 * Header menu link wrapper (for link styling)
 */
const HeaderMenuLinkSC = styled.span`
  a {
    display: block;
    color: white;
    text-decoration: none;
    text-transform: uppercase;
    border-bottom: 2px solid transparent;

    transition: transform 0.05s, border 0.2s;

    &:hover {
      border-bottom-color: ${theme.palette.secondary.light};
      transform: scale(1.05);
    }
  }
`;

export { HeaderMenuLinkSC };

/* eslint import/prefer-default-export: off */
