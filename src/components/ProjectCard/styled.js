import styled from 'styled-components';
import Img from 'gatsby-image';
import Link from 'gatsby-link';

/**
 * Project card
 */
const ProjectCardSC = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  margin-bottom: 24px;
  padding: 4px;
  background-color: white;
  border-radius: 2px;
  box-shadow: ${({ theme }) => theme.shadows[2]};
  transition: transform 0.2s ease-in-out, box-shadow 0.2s ease-in-out;

  &:hover {
    transform: scale(1.025);
    box-shadow: ${({ theme }) => theme.shadows[4]};
  }
`;

/**
 * Project description row
 */
const ProjectDescriptionSC = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 4px;
  padding-top: 8px;
`;

/**
 * Project image
 */
const ProjectImageSC = styled(Img)`
  img {
    margin: 0;
  }
`;

/**
 * Project image placeholder (with no image)
 */
const ProjectImagePlaceholderSC = styled.div`
  flex-grow: 1;
  margin: 0;
  border: 1px solid darkgrey;
  background-color: lightgrey;
`;

/**
 * Project card link button
 */
const ProjectLinkSC = styled(Link)`
  margin-left: auto;
  color: ${({ theme }) => theme.palette.blue.main};
`;

/**
 * Project name in description
 */
const ProjectNameSC = styled.span`
  font-weight: bold;
`;
/**
 * Project year in description
 */
const ProjectYearSC = styled.span`
  font-style: italic;
  font-size: 90%;
`;

export {
  ProjectCardSC,
  ProjectDescriptionSC,
  ProjectImageSC,
  ProjectImagePlaceholderSC,
  ProjectLinkSC,
  ProjectNameSC,
  ProjectYearSC,
};
