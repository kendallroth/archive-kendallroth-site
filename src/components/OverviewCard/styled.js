import styled, { css } from 'styled-components';

/**
 * Information card wrapper
 */
const OverviewCardSC = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1em;
  background-color: rgba(255, 255, 255, 0.95);
  border-radius: 2px;
  box-shadow: ${({ theme }) => theme.shadows[2]};
`;

/**
 * Card content text
 */
const OverviewCardTextSC = styled.div`
  font-size: 0.9em;
`;

/**
 * Card content title
 */
const OverviewCardTitleSC = styled.div`
  margin-bottom: 0.5em;
  text-align: center;
  font-size: 1.25em;
  font-weight: bold;
`;

export { OverviewCardSC, OverviewCardTextSC, OverviewCardTitleSC };
