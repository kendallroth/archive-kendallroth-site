/**
 * Adapted from: https://codepen.io/plavookac/pen/QMwObb
 */

import styled from 'styled-components';

// Assets
import waveBottomImage from '@assets/images/wave-bot.png';
import waveMiddleImage from '@assets/images/wave-mid.png';
import waveTopImage from '@assets/images/wave-top.png';

/**
 * Enable different styles per wave
 * @param  {string} position - Wave "z" position
 * @return {string}          - Wave styles
 */
const getWaveStyles = (position) => {
  switch (position) {
    case 'top':
      return `
        opacity: 0.2;
        z-index: 3;
      `;
    case 'mid':
      return `
        opacity: 0.4;
        z-index: 2;
      `;
    case 'bot':
      return `
        opacity: 0.6;
        z-index: 1;
      `;
    default:
      return '';
  }
};

/**
 * Enable different styles per wave image
 * @param  {string} position - Wave image "z" position
 * @return {string}          - Wave image styles
 */
const getWaveImageStyles = (position) => {
  switch (position) {
    case 'top':
      return `
        animation: move_wave 20s linear infinite;
        background-image: url(${waveTopImage});
        background-size: 50% 100px;
      `;
    case 'mid':
      return `
        background-image: url(${waveMiddleImage});
        background-size: 50% 120px;
      `;
    case 'bot':
      return `
        animation: move_wave 35s linear infinite;
        background-image: url(${waveBottomImage});
        background-size: 50% 100px;
      `;
    default:
      return '';
  }
};

/**
 * Wave container
 */
const BottomWavesSC = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  /* Animation for waves */
  @keyframes move_wave {
    0% {
      transform: translateX(0) translateZ(0) scaleY(1);
    }
    50% {
      transform: translateX(-25%) translateZ(0) scaleY(0.55);
    }
    100% {
      transform: translateX(-50%) translateZ(0) scaleY(1);
    }
  }
`;

/**
 * Background wave
 */
const BottomWaveSC = styled.div`
  position: absolute;
  width: 100%;
  overflow: hidden;
  height: 100%;
  bottom: -1px;

  ${({ position }) => getWaveStyles(position)}
`;

/**
 * Background wave image
 */
const BottomWaveImageSC = styled.div`
  position: absolute;
  left: 0;
  width: 200%;
  height: 100%;
  background-repeat: repeat no-repeat;
  background-position: 0 bottom;
  transform-origin: center bottom;

  ${({ position }) => getWaveImageStyles(position)}
`;

export { BottomWaveSC, BottomWavesSC, BottomWaveImageSC };
