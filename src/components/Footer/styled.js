import styled from 'styled-components';

/**
 * Page footer section
 */
const FooterSC = styled.footer`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${({ theme }) => theme.spacing * 5}px;
  color: white;
  background-color: black;
`;

// Page footer attributions
const FooterAttributionsSC = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: ${({ theme }) => theme.spacing * 2}px;
  opacity: 0.6;

  a {
    margin-left: ${({ theme }) => theme.spacing / 2}px;
    color: ${({ theme }) => theme.palette.primary.lightest};
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }

  i {
    margin: 0 ${({ theme }) => theme.spacing / 2}px;
    font-size: 100%;
    color: ${({ theme }) => theme.palette.secondary.light};
  }
`;

/**
 * Footer copyright text
 */
const FooterCopyrightSC = styled.div`
  text-align: center;
  opacity: 0.8;
`;

/**
 * Footer social media icon bar wrapper
 */
const FooterIconBarSC = styled.nav``;

export { FooterSC, FooterAttributionsSC, FooterCopyrightSC, FooterIconBarSC };
