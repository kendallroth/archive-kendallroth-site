import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';

// Components
import IconBar from '@components/IconBar';

// Utils
import gitHubLogo from '@assets/icons/icon_github_light.svg';
import gitLabLogo from '@assets/icons/icon_gitlab_light.svg';
import linkedInLogo from '@assets/icons/icon_linkedin_light.svg';

// Styles
import {
  FooterSC,
  FooterAttributionsSC,
  FooterCopyrightSC,
  FooterIconBarSC,
} from './styled';

const socialMediaIcons = [
  { name: 'GitHub', img: gitHubLogo, url: 'https://github.com/kendallroth' },
  { name: 'GitLab', img: gitLabLogo, url: 'https://gitlab.com/kendallroth' },
  {
    name: 'LinkedIn',
    img: linkedInLogo,
    url: 'https://linkedin.com/in/kendallroth',
  },
];

/**
 * Index page
 */
const IndexPage = (props) => {
  const { showSocial } = props;

  const theme = useContext(ThemeContext);

  return (
    <FooterSC>
      <FooterIconBarSC>
        {showSocial && (
          <IconBar
            color="#3a3a3a"
            icons={socialMediaIcons}
            round={false}
            size={theme.spacing * 3}
          />
        )}
        <FooterCopyrightSC>
          &copy; Kendall Roth {new Date().getFullYear()}
        </FooterCopyrightSC>
        <FooterAttributionsSC>
          Made with <i className="material-icons">favorite</i> from scratch
          using <a href="https://gatsbyjs.org">Gatsby</a>
        </FooterAttributionsSC>
      </FooterIconBarSC>
    </FooterSC>
  );
};

IndexPage.propTypes = {
  showSocial: PropTypes.bool,
};

IndexPage.defaultProps = {
  showSocial: false,
};

export default IndexPage;
