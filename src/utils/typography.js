import Typography from 'typography';

const typography = new Typography({
  baseFontSize: '16px',
  bodyFontFamily: ['Raleway'],
  googleFonts: [
    {
      name: 'Raleway',
      styles: ['400', '500', '700'],
    },
  ],
  headerFontFamily: ['Raleway'],
  includeNormalize: true,
});

export default typography;
