import { css } from 'styled-components';

// Each device begins at the specified value/width
//  (ie. "desktop" is from 992 - 1200 px)
const QuerySizes = {
  huge: 2560,
  large: 1200,
  desktop: 992,
  tablet: 768,
  small: 576,
  phone: 0,
};

// Idea taken from styled-components.com
//  https://github.com/styled-components/styled-components/blob/master/docs/tips-and-tricks.md#media-templates
const MediaQuery = Object.keys(QuerySizes).reduce((accumulator, query, idx, array) => {
  const size = QuerySizes[query];

  // Get parent query and size (ie. a tablet is from 768 - 992, this will return '992')
  const parentQuery = array[idx - 1];
  const parentSize = QuerySizes[parentQuery];

  // Create an object with all media query types
  const MediaQueryStrings = {};

  // An undefined parentSize indicates that there is no "larger" parent query size.
  //  This means that "largeDown" should apply to everything (it is the top size)
  //  Additionally, this means that "largeOnly" and "largeUp" are technically the same thing

  MediaQueryStrings[`${query}Up`] =
    parentSize !== undefined ? `(min-width: ${size}px)` : `(min-width: ${size}px)`;

  MediaQueryStrings[`${query}Down`] =
    parentSize !== undefined ? `(max-width: ${parentSize - 1}px)` : '(min-width: 0px)';

  MediaQueryStrings[`${query}Only`] =
    parentSize !== undefined
      ? `(min-width: ${size}px) and (max-width: ${parentSize - 1}px)`
      : `(min-width: ${size}px)`;

  // Generate the media queries dynamically
  //  Each query type (up, down, only) has its associated media query strings (above)
  //  This creates a media query for that range and adds it to the MediaQuery object
  Object.keys(MediaQueryStrings).forEach((key) => {
    // eslint-disable-next-line no-param-reassign
    accumulator[key] = (...args) => css`
      @media ${MediaQueryStrings[key]} {
        ${css(...args)};
      }
    `;
  });

  // The final output is an object with a media query for each breakpoint and type
  //  Usage:   ${MediaQuery.largeUp`background-color: red;`};
  return accumulator;
}, {});

export { MediaQuery, QuerySizes };
