const React = require('react');
const PropTypes = require('prop-types');

// Components
const { default: AppWrapper } = require('./gatsby-shared');

/**
 * Wrap the React app with the styled components Theme Provider
 */
const WrappedRoot = ({ element }) => {
  return <AppWrapper>{element}</AppWrapper>;
};

WrappedRoot.propTypes = {
  element: PropTypes.element.isRequired,
};

exports.wrapRootElement = WrappedRoot;
